import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Import data
df = pd.read_csv("turn-on.csv", header=10)

fig, ax = plt.subplots()
ax.plot(df["Source[s]"]*1e6, (df["CH1[V]"]+0.3)*10, "k", label="Vin")
ax.plot(df["Source[s]"]*1e6, (df["CH2[V]"]+1.5)*10, "#666666",label="Vout")
ax.grid()
ax.set(xlabel="Time [us]", ylabel="Input voltage [V]")
ax.legend()
plt.axvline(x=268+42, color='k')
plt.axvline(x=268+232, color='k')

fig.savefig("turn-on.png")
plt.show()

# Second image
plt.figure()
df = pd.read_csv("turn-off.csv", header=10)

fig, ax = plt.subplots()
ax.plot(df["Source[s]"]*1e6, (df["CH1[V]"]+0.3)*10, "k", label="Vin")
ax.plot(df["Source[s]"]*1e6, (df["CH2[V]"]+1.5)*10, "#666666",label="Vout")
ax.grid()
ax.set(xlabel="Time [us]", ylabel="Input voltage [V]")
ax.legend()
plt.axvline(x=20, color='k')
plt.axvline(x=20+26, color='k')

fig.savefig("turn-off.png")
plt.show()
